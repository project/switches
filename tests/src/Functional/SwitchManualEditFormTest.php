<?php

namespace Drupal\Tests\switches\Functional;

use Drupal\switches\Entity\SwitchEntity;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the switches edit form - manual activation.
 *
 * @group switches
 */
class SwitchManualEditFormTest extends BrowserTestBase {

  /**
   * The admin URL path for managing switches.
   *
   * @var string
   */
  protected const ADMIN_URL = 'admin/structure/switch';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'switches',
  ];

  /**
   * Tests if the switch manual activation status can be set to false.
   */
  public function testSwitchCanBeDisabled(): void {
    $switchId = strtolower($this->randomMachineName());

    $values = [
      'id' => $switchId,
      'label' => $this->randomString(),
      'activationMethod' => 'manual',
      'activationConditions' => [],
      'manualActivationStatus' => TRUE,
    ];
    /** @var \Drupal\switches\Entity\SwitchEntity $enabledSwitch */
    $enabledSwitch = SwitchEntity::create($values);
    $enabledSwitch->save();

    /** @var \Drupal\Core\Entity\EntityStorageInterface $switchStorage */
    $switchStorage = $this->container->get('entity_type.manager')
      ->getStorage('switch');

    /** @var \Drupal\switches\Entity\SwitchEntity $switch */
    $switch = $switchStorage->load($switchId);
    $this->assertEquals($switch->getManualActivationStatus(), TRUE, 'Initial switch status should be true.');

    $account = $this->createUser(['administer switches']);

    $this->drupalLogin($account);
    $this->drupalGet(self::ADMIN_URL . '/' . $switchId . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->checkboxChecked('manualActivationStatus');

    $page = $this->getSession()->getPage();
    $page->uncheckField('manualActivationStatus');
    $page->pressButton('Save');

    $this->assertSession()->statusCodeEquals(200);

    /** @var \Drupal\switches\Entity\SwitchEntity $switch */
    $switch = $switchStorage->load($switchId);
    $this->assertEquals($switch->getManualActivationStatus(), FALSE, 'Switch status was expected to be false.');

    $this->drupalGet(self::ADMIN_URL . '/' . $switchId . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->checkboxNotChecked('manualActivationStatus');
  }

}

<?php

namespace Drupal\switches\TwigExtension;

use Drupal\switches\SwitchManagerInterface;
use Psr\Log\LoggerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Switches Twig extension that adds a custom function to check a switch status.
 *
 * @code
 * {{ switch_is_active($my_switch_id) }}
 * @endcode
 */
class SwitchExtension extends AbstractExtension {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Switch Manager service.
   *
   * @var \Drupal\switches\SwitchManagerInterface
   */
  protected $switchManager;

  /**
   * Constructs the SwitchExtension object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\switches\SwitchManagerInterface $switchManager
   *   The Switch Manager service.
   */
  public function __construct(LoggerInterface $logger, SwitchManagerInterface $switchManager) {
    $this->logger = $logger;
    $this->switchManager = $switchManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('switch_is_active', [$this, 'switchIsActive']),
    ];
  }

  /**
   * Returns Activation status of switch if it exists.
   *
   * @param string $switchId
   *   The machine name for the switch.
   *
   * @return bool
   *   The switch activation status or the configured default for missing
   *   switches.
   *
   * @see SwitchManagerInterface::getActivationStatus()
   */
  public function switchIsActive(string $switchId): bool {
    return $this->switchManager->getActivationStatus($switchId);
  }

}

<?php

namespace Drupal\switches\Exception;

/**
 * Exception thrown when an undefined Switch is requested.
 */
class MissingSwitchException extends \Exception {

}
